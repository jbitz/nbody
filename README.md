nbody.py
=================== 
nbody.py is a Python script that simulates the motion of charged particles according to Coulomb's Law. It was written for an extra credit assignment for Stanford's PHYSICS 43 class. Requires graphics.py, which is included with the code here.

![Image of simulation with four particles](https://i.imgur.com/M14YcDU.png)

